import Vector2 from "./Vector2.js";
import World from "./World.js";

export default class Body {
    constructor(position, mass, coeffRest) {
        this.position = new Vector2(position.x, position.y);
        this.mass = mass;
        this.coeffRest = coeffRest;
        this.velocity = new Vector2(0, 0);
        this.angularVel = 0;
        this.angle = 0;
        this.angularAcc = 0;
    }

    addForce(force) {
        this.velocity = this.velocity.add(force);
    }

    addTorque(torque) {
        this.angularAcc += torque / this.momentInert;
    }

    update(deltaTime) {
        this.velocity = this.velocity.add(new Vector2(0, World.gravity.y * this.mass).mulConst(deltaTime));
        this.position = this.position.add(this.velocity.mulConst(deltaTime));
        this.velocity = this.velocity.mulConst(1 - this.groundFriction); // Apply ground friction
        this.angularVel += this.angularAcc * deltaTime;
        this.angle += this.angularVel * deltaTime;
        this.angularAcc = 0;
    }

    _isCollidingWith(other) {
        // Default to false, should be overridden in subclasses
        return false;
    }

    _resolveCollision(other) {
        // Default, should be overridden in subclasses
    }
}
