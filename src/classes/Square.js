import Body from "./Body.js";


export default class Square extends Body {
    constructor({ position, mass, coeffRest, sideLength, airFriction, groundFriction }) {
        super(position, mass, coeffRest);
        this.sideLength = sideLength;
        this.airFriction = airFriction;
        this.groundFriction = groundFriction;
        this.momentInert = (1 / 6) * this.mass * this.sideLength * this.sideLength;
        this.angle = 0; // Initialize angle for rotation
    }

    _isCollidingWith(other) {
        if (other instanceof Circle) {
            return this._isCollidingWithCircle(other);
        } else if (other instanceof Square) {
            return this._isCollidingWithSquare(other);
        } else if (other instanceof Triangle) {
            return this._isCollidingWithTriangle(other);
        }
        return false;
    }

    _isCollidingWithCircle(other) {
        const closestX = Math.max(this.position.x - this.sideLength / 2, Math.min(other.position.x, this.position.x + this.sideLength / 2));
        const closestY = Math.max(this.position.y - this.sideLength / 2, Math.min(other.position.y, this.position.y + this.sideLength / 2));
        const distanceX = other.position.x - closestX;
        const distanceY = other.position.y - closestY;
        return (distanceX * distanceX + distanceY * distanceY) < (other.radius * other.radius);
    }

    _isCollidingWithSquare(other) {
        return !(this.position.x + this.sideLength / 2 < other.position.x - other.sideLength / 2 ||
                 this.position.x - this.sideLength / 2 > other.position.x + other.sideLength / 2 ||
                 this.position.y + this.sideLength / 2 < other.position.y - other.sideLength / 2 ||
                 this.position.y - this.sideLength / 2 > other.position.y + other.sideLength / 2);
    }

    _isCollidingWithTriangle(other) {
        // Approximation: Square vs Triangle collision detection
        // Can be improved
        for (let i = 0; i < other.vertices.length; i++) {
            const next = (i + 1) % other.vertices.length;
            if (this._isPointInSquare(other.vertices[i])) {
                return true;
            }
        }
        return false;
    }

    _isPointInSquare(point) {
        return point.x >= this.position.x - this.sideLength / 2 &&
               point.x <= this.position.x + this.sideLength / 2 &&
               point.y >= this.position.y - this.sideLength / 2 &&
               point.y <= this.position.y + this.sideLength / 2;
    }

    _resolveCollisionCircle(other) {
        // Placeholder for Square vs Circle collision resolution
        // Basic separation and impulse handling
        const normal = other.position.sub(this.position).normalize();
        const overlap = other.radius - (this.sideLength / 2);
        const separation = normal.mulConst(overlap);

        this.position = this.position.sub(separation);
        // Note: Add handling of impulse here
    }

    _resolveCollisionSquare(other) {
        // Placeholder for Square vs Square collision resolution
        // Basic separation and impulse handling
        const normal = this.position.sub(other.position).normalize();
        const overlap = this.sideLength - other.sideLength;
        const separation = normal.mulConst(overlap);

        this.position = this.position.add(separation);
        // Note: Add handling of impulse here
    }

    _resolveCollisionTriangle(other) {
        // Placeholder for Square vs Triangle collision resolution
        // Basic separation and impulse handling
        const normal = this.position.sub(other.position).normalize();
        const overlap = this.sideLength - other.sideLength;
        const separation = normal.mulConst(overlap);

        this.position = this.position.add(separation);
        // Note: Add handling of impulse here
    }

    update(deltaTime, canvas, color, spatialGrid) {
        const airResistance = this.velocity.mulConst(-this.airFriction);
        this.addForce(airResistance);

        // Boundary conditions
        if (this.position.x - this.sideLength / 2 < 0 || this.position.x + this.sideLength / 2 > canvas.width) {
            this.velocity.x *= -this.coeffRest;
            this.position.x = Math.max(this.sideLength / 2, Math.min(canvas.width - this.sideLength / 2, this.position.x));
        }

        if (this.position.y - this.sideLength / 2 < 0 || this.position.y + this.sideLength / 2 > canvas.height) {
            this.velocity.y *= -this.coeffRest;
            this.position.y = Math.max(this.sideLength / 2, Math.min(canvas.height - this.sideLength / 2, this.position.y));
        }

        if (this.position.y + this.sideLength / 2 >= canvas.height) {
            this.velocity.x *= (1 - this.groundFriction);
        }

        super.update(deltaTime);

        // Check for collisions
        const nearbyObjects = spatialGrid.getNearbyObjects(this.position);
        for (let other of nearbyObjects) {
            if (other !== this && this._isCollidingWith(other)) {
                this._resolveCollision(other);
            }
        }

        this.draw(canvas.getContext("2d"), color);
    }

    draw(c, color) {
        // Draw the square
        c.beginPath();
        c.strokeStyle = "black";
        c.lineWidth = 1;
        c.rect(this.position.x - this.sideLength / 2, this.position.y - this.sideLength / 2, this.sideLength, this.sideLength);
        c.fillStyle = color;
        c.fill();
        c.stroke();
    }
}
