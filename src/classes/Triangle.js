import Body from './Body.js';
import Vector2 from './Vector2.js';
import Circle from './Circle.js';
import Square from './Square.js';

export default class Triangle extends Body {
    constructor({ position, mass, coeffRest, vertices, airFriction, groundFriction }) {
        super(position, mass, coeffRest);
        this.originalVertices = vertices; // Conserver les sommets d'origine
        this.vertices = this.originalVertices.map(v => v.add(position));
        this.airFriction = airFriction;
        this.groundFriction = groundFriction;
        this.angle = 0; // Angle de rotation initial
        this.velocity = new Vector2(0, 0); // Initialiser la vitesse
        this.force = new Vector2(0, 0); // Initialiser la force
        this.debug = true; // Activer les messages de débogage
    }

    updateVertices() {
        const cosTheta = Math.cos(this.angle);
        const sinTheta = Math.sin(this.angle);

        this.vertices = this.originalVertices.map(vertex => {
            const relativeVertex = vertex.sub(this.position);
            return new Vector2(
                this.position.x + cosTheta * relativeVertex.x - sinTheta * relativeVertex.y,
                this.position.y + sinTheta * relativeVertex.x + cosTheta * relativeVertex.y
            );
        });
    }

    // Gestion des collisions
    _isCollidingWith(other) {
        if (other instanceof Circle) {
            return this._isCollidingWithCircle(other);
        } else if (other instanceof Square) {
            return this._isCollidingWithSquare(other);
        } else if (other instanceof Triangle) {
            return this._isCollidingWithTriangle(other);
        }
        return false;
    }

    _isCollidingWithCircle(other) {
        for (let i = 0; i < this.vertices.length; i++) {
            const next = (i + 1) % this.vertices.length;
            const edge = this.vertices[next].sub(this.vertices[i]);
            const toCircle = other.position.sub(this.vertices[i]);
            const projection = toCircle.dot(edge.normalize());

            if (projection < 0 || projection > edge.magnitude()) {
                return false;
            }

            const closestPoint = this.vertices[i].add(edge.normalize().mulConst(projection));
            const distance = closestPoint.sub(other.position).magnitude();
            if (distance < other.radius) {
                return true;
            }
        }
        return false;
    }

    _isCollidingWithSquare(other) {
        const squareVertices = [
            new Vector2(other.position.x - other.sideLength / 2, other.position.y - other.sideLength / 2),
            new Vector2(other.position.x + other.sideLength / 2, other.position.y - other.sideLength / 2),
            new Vector2(other.position.x + other.sideLength / 2, other.position.y + other.sideLength / 2),
            new Vector2(other.position.x - other.sideLength / 2, other.position.y + other.sideLength / 2)
        ];

        for (let i = 0; i < this.vertices.length; i++) {
            const next = (i + 1) % this.vertices.length;
            if (this._isPointInSquare(this.vertices[i], squareVertices)) {
                return true;
            }
        }
        return false;
    }

    _isPointInSquare(point, squareVertices) {
        const [v0, v1, v2, v3] = squareVertices;
        const isInXBounds = point.x >= Math.min(v0.x, v2.x) && point.x <= Math.max(v0.x, v2.x);
        const isInYBounds = point.y >= Math.min(v0.y, v2.y) && point.y <= Math.max(v0.y, v2.y);
        return isInXBounds && isInYBounds;
    }

    _isCollidingWithTriangle(other) {
        for (let i = 0; i < this.vertices.length; i++) {
            const next = (i + 1) % this.vertices.length;
            if (this._isPointInTriangle(other.vertices[i])) {
                return true;
            }
        }
        return false;
    }

    _isPointInTriangle(point) {
        const [v0, v1, v2] = this.vertices;
        const d00 = v1.sub(v0).dot(v1.sub(v0));
        const d01 = v1.sub(v0).dot(v2.sub(v0));
        const d11 = v2.sub(v0).dot(v2.sub(v0));
        const d20 = point.sub(v0).dot(v1.sub(v0));
        const d21 = point.sub(v0).dot(v2.sub(v0));
        const denom = d00 * d11 - d01 * d01;
        const v = (d11 * d20 - d01 * d21) / denom;
        const w = (d00 * d21 - d01 * d20) / denom;
        return v >= 0 && w >= 0 && (v + w) <= 1;
    }

    _resolveCollisionCircle(other) {
        const normal = other.position.sub(this.position).normalize();
        const overlap = other.radius - this.vertices.reduce((min, vertex) => Math.min(min, vertex.sub(other.position).magnitude()), Infinity);
        const separation = normal.mulConst(overlap);

        this.position = this.position.sub(separation);
        this.velocity = this.velocity.mulConst(-this.coeffRest); // Réglage de la vitesse après collision

        // Debug
        if (this.debug) {
            console.log("Collision avec un cercle : ", {
                normal: normal,
                overlap: overlap,
                separation: separation,
                newPosition: this.position,
                newVelocity: this.velocity
            });
        }
    }

    _resolveCollisionSquare(other) {
        const normal = this.position.sub(other.position).normalize();
        const overlap = this.vertices.reduce((min, vertex) => Math.min(min, vertex.sub(other.position).magnitude()), Infinity);
        const separation = normal.mulConst(overlap);

        this.position = this.position.sub(separation);
        this.velocity = this.velocity.mulConst(-this.coeffRest); // Réglage de la vitesse après collision

        // Debug
        if (this.debug) {
            console.log("Collision avec un carré : ", {
                normal: normal,
                overlap: overlap,
                separation: separation,
                newPosition: this.position,
                newVelocity: this.velocity
            });
        }
    }

    _resolveCollisionTriangle(other) {
        const normal = this.position.sub(other.position).normalize();
        const overlap = this.vertices.reduce((min, vertex) => Math.min(min, vertex.sub(other.position).magnitude()), Infinity);
        const separation = normal.mulConst(overlap);

        this.position = this.position.sub(separation);
        this.velocity = this.velocity.mulConst(-this.coeffRest); // Réglage de la vitesse après collision

        // Debug
        if (this.debug) {
            console.log("Collision avec un triangle : ", {
                normal: normal,
                overlap: overlap,
                separation: separation,
                newPosition: this.position,
                newVelocity: this.velocity
            });
        }
    }

    update(deltaTime, canvas, color, spatialGrid) {
        // Appliquer la force
        const airResistance = this.velocity.mulConst(-this.airFriction);
        this.addForce(airResistance);
    
        // Calculer l'accélération
        const acceleration = this.force.mulConst(1 / this.mass);
        this.velocity = this.velocity.add(acceleration.mulConst(deltaTime));
        this.position = this.position.add(this.velocity.mulConst(deltaTime));
    
        // Conditions limites avec une marge
        const margin = 1;
        if (this.position.x < margin || this.position.x > canvas.width - margin) {
            this.velocity.x *= -this.coeffRest;
            this.position.x = Math.max(margin, Math.min(canvas.width - margin, this.position.x));
        }
    
        if (this.position.y < margin || this.position.y > canvas.height - margin) {
            this.velocity.y *= -this.coeffRest;
            this.position.y = Math.max(margin, Math.min(canvas.height - margin, this.position.y));
        }
    
        // Mettre à jour les sommets
        this.updateVertices();
    
        // Détection des collisions
        const nearbyObjects = spatialGrid.getNearbyObjects(this.position);
        for (let other of nearbyObjects) {
            if (other !== this && this._isCollidingWith(other)) {
                this._resolveCollision(other);
            }
        }
    
        // Dessiner le triangle
        this.draw(canvas.getContext("2d"), color);
    
        // Debug
        if (this.debug) {
            console.log("Position : ", this.position);
            console.log("Vitesse : ", this.velocity);
            console.log("Forces appliquées : ", this.force);
        }
    }
    

    draw(c, color) {
        c.beginPath();
        c.strokeStyle = "black";
        c.lineWidth = 1;
        c.moveTo(this.vertices[0].x, this.vertices[0].y);
        c.lineTo(this.vertices[1].x, this.vertices[1].y);
        c.lineTo(this.vertices[2].x, this.vertices[2].y);
        c.closePath();
        c.fillStyle = color;
        c.fill();
        c.stroke();
    }
}
