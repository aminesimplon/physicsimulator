import Body from "./Body.js";

export default class Circle extends Body {
    constructor({ position, mass, coeffRest, radius, airFriction, groundFriction }) {
        super(position, mass, coeffRest);
        this.radius = radius;
        this.airFriction = airFriction;
        this.groundFriction = groundFriction;
        this.momentInert = (1 / 2) * this.mass * this.radius * this.radius;
        this.angularVel = 0;
    }

    _isCollidingWith(other) {
        if (other instanceof Circle) {
            return this._isCollidingWithCircle(other);
        } else if (other instanceof Square) {
            return this._isCollidingWithSquare(other);
        } else if (other instanceof Triangle) {
            return this._isCollidingWithTriangle(other);
        }
        return false;
    }

    _isCollidingWithCircle(other) {
        const distance = this.position.sub(other.position).magnitude();
        return distance < this.radius + other.radius;
    }

    _isCollidingWithSquare(other) {
        const closestX = Math.max(other.position.x - other.sideLength / 2, Math.min(this.position.x, other.position.x + other.sideLength / 2));
        const closestY = Math.max(other.position.y - other.sideLength / 2, Math.min(this.position.y, other.position.y + other.sideLength / 2));
        const distanceX = this.position.x - closestX;
        const distanceY = this.position.y - closestY;
        return (distanceX * distanceX + distanceY * distanceY) < (this.radius * this.radius);
    }

    _isCollidingWithTriangle(other) {
        for (let i = 0; i < other.vertices.length; i++) {
            const next = (i + 1) % other.vertices.length;
            if (this._isPointInTriangle(other.vertices[i], other.vertices[next])) {
                return true;
            }
        }
        return false;
    }

    _isPointInTriangle(point, v0, v1) {
        const d1 = (point.x - v1.x) * (v0.y - v1.y) - (v0.x - v1.x) * (point.y - v1.y);
        const d2 = (point.x - v0.x) * (v1.y - v0.y) - (v1.x - v0.x) * (point.y - v0.y);
        return d1 * d2 >= 0 && d1 * d1 + d2 * d2 <= (v1.x - v0.x) * (v1.x - v0.x) + (v1.y - v0.y) * (v1.y - v0.y);
    }

    _resolveCollision(other) {
        if (other instanceof Circle) {
            this._resolveCollisionCircle(other);
        } else if (other instanceof Square) {
            this._resolveCollisionSquare(other);
        } else if (other instanceof Triangle) {
            this._resolveCollisionTriangle(other);
        }
    }

    _resolveCollisionCircle(other) {
        const normal = this.position.sub(other.position).normalize();
        const relativeVelocity = this.velocity.sub(other.velocity);
        const speed = relativeVelocity.dot(normal);

        if (speed > 0) return; // Ignore if moving apart

        const impulseMagnitude = (2 * speed) / (this.mass + other.mass);
        const impulse = normal.mulConst(impulseMagnitude);

        this.velocity = this.velocity.sub(impulse.mulConst(other.mass));
        other.velocity = other.velocity.add(impulse.mulConst(this.mass));

        // Separate overlapping circles
        const overlap = (this.radius + other.radius) - this.position.sub(other.position).magnitude();
        const separation = normal.mulConst(overlap / 2);

        this.position = this.position.add(separation);
        other.position = other.position.sub(separation);
    }

    _resolveCollisionSquare(other) {
        // Placeholder for Circle vs Square collision resolution
        // Basic separation and impulse handling
        const normal = this.position.sub(other.position).normalize();
        const overlap = this.radius - other.sideLength / 2;
        const separation = normal.mulConst(overlap);

        this.position = this.position.add(separation);
        // Note: Add handling of impulse here
    }

    _resolveCollisionTriangle(other) {
        // Placeholder for Circle vs Triangle collision resolution
        // Basic separation and impulse handling
        const normal = this.position.sub(other.position).normalize();
        const overlap = this.radius - this.position.sub(other.position).magnitude();
        const separation = normal.mulConst(overlap);

        this.position = this.position.add(separation);
        // Note: Add handling of impulse here
    }

    update(deltaTime, canvas, color, spatialGrid) {
        const airResistance = this.velocity.mulConst(-this.airFriction);
        this.addForce(airResistance);

        // Boundary conditions
        if (this.position.x - this.radius < 0 || this.position.x + this.radius > canvas.width) {
            this.velocity.x *= -this.coeffRest;
            this.position.x = Math.max(this.radius, Math.min(canvas.width - this.radius, this.position.x));
        }

        if (this.position.y - this.radius < 0 || this.position.y + this.radius > canvas.height) {
            this.velocity.y *= -this.coeffRest;
            this.position.y = Math.max(this.radius, Math.min(canvas.height - this.radius, this.position.y));
        }

        if (this.position.y + this.radius >= canvas.height) {
            this.velocity.x *= (1 - this.groundFriction);
        }

        super.update(deltaTime);

        // Check for collisions
        const nearbyObjects = spatialGrid.getNearbyObjects(this.position);
        for (let other of nearbyObjects) {
            if (other !== this && this._isCollidingWith(other)) {
                this._resolveCollision(other);
            }
        }

        this.draw(canvas.getContext("2d"), color);
    }

    draw(c, color) {
        // Draw the circle
        c.beginPath();
        c.strokeStyle = "black";
        c.lineWidth = 1;
        c.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
        c.fillStyle = color;
        c.fill();
        c.stroke();

        // Draw the red line indicating rotation
        const rotationLength = this.radius; // Length of the rotation indicator line
        const endX = this.position.x + Math.cos(this.angle) * rotationLength;
        const endY = this.position.y + Math.sin(this.angle) * rotationLength;
        c.beginPath();
        c.strokeStyle = "red";
        c.lineWidth = 1;
        c.moveTo(this.position.x, this.position.y);
        c.lineTo(endX, endY);
        c.stroke();
    }
}
