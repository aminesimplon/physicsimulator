export default class Vector2 {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    static zero = new Vector2(0, 0);

    add(other) {
        return new Vector2(this.x + other.x, this.y + other.y);
    }

    sub(other) {
        return new Vector2(this.x - other.x, this.y - other.y);
    }

    mulConst(value) {
        return new Vector2(this.x * value, this.y * value);
    }

    divConst(value) {
        return new Vector2(this.x / value, this.y / value);
    }

    dot(other) {
        return this.x * other.x + this.y * other.y;
    }

    cross(other) {
        return this.x * other.y - this.y * other.x;
    }

    magnitude() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    normalize() {
        const mag = this.magnitude();
        return mag > 0 ? new Vector2(this.x / mag, this.y / mag) : Vector2.zero;
    }
}
