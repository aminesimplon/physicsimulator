export default class SpatialGrid {
    constructor(cellSize) {
        this.cellSize = cellSize;
        this.cells = new Map();
    }

    addObject(object) {
        const cellKey = this._getCellKey(object.position);
        if (!this.cells.has(cellKey)) {
            this.cells.set(cellKey, []);
        }
        this.cells.get(cellKey).push(object);
    }

    getNearbyObjects(position) {
        const cellKey = this._getCellKey(position);
        const [x, y] = cellKey.split(',').map(Number);
        const nearbyObjects = [];
        for (let dx = -1; dx <= 1; dx++) {
            for (let dy = -1; dy <= 1; dy++) {
                const key = `${x + dx},${y + dy}`;
                if (this.cells.has(key)) {
                    nearbyObjects.push(...this.cells.get(key));
                }
            }
        }
        return nearbyObjects;
    }

    _getCellKey(position) {
        const x = Math.floor(position.x / this.cellSize);
        const y = Math.floor(position.y / this.cellSize);
        return `${x},${y}`;
    }

    clear() {
        this.cells.clear();
    }
}
