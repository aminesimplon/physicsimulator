import Circle from "./classes/Circle.js";
import Square from "./classes/Square.js";
import Triangle from "./classes/Triangle.js";
import Vector2 from "./classes/Vector2.js";
import SpatialGrid from "./classes/SpatialGrid.js";
import World from "./classes/World.js";

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const shapes = [
    new Circle({
        position: { x: 100, y: 100 },
        mass: 50 / 1000,
        coeffRest: .5,
        radius: 25,
        airFriction: 0,
        groundFriction: 0.01
    }),
    new Square({
        position: { x: 300, y: 100 },
        mass: 50 / 1000,
        coeffRest: .5,
        sideLength: 50,
        airFriction: 0,
        groundFriction: 0.01
    }),
    new Triangle({
        position: { x: 500, y: 100 },
        mass: 50 / 1000,
        coeffRest: .5,
        vertices: [
            new Vector2(0, -25),
            new Vector2(-25, 25),
            new Vector2(25, 25)
        ],
        airFriction: 0,
        groundFriction: 0.01
    })
];

shapes[0].addForce(new Vector2(10, 0));
shapes[2].addForce(new Vector2(10, 0));

const spatialGrid = new SpatialGrid(100);

function animate() {
    setTimeout(animate, 10);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    spatialGrid.clear();

    for (let shape of shapes) {
        shape.addForce(new Vector2(0, World.gravity.y * shape.mass));
        spatialGrid.addObject(shape);
    }

    for (let shape of shapes) {
        shape.update(0.1, canvas, "black", spatialGrid);
    }
}

animate();
